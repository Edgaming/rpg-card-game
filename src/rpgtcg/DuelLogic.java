/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgtcg;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//CREATE .applyAttack() func in Card that returns the card with updated health/effect turns
/**
 * A CLASS THAT FUCKING DUELS TWO NIGGAS. DEAL WITH IT BITCHES
 *
 * @author Moody
 */
public class DuelLogic {

    private Player USER, CPU, turn, winner;
    private BufferedReader br;
    private InputStreamReader in;

    public DuelLogic(Player USER, Player CPU) {
        this.USER = USER;
        this.CPU = CPU;
        prepareDuel();
    }

    /**
     * Randomizes, and returns a number, between the specified parameters, min
     * and max.
     *
     * @param min The lower bound.
     * @param max The upper bound.
     * @return A randomized int between the lower and upper bounds.
     */
    private int randomize(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    /**
     * Randomizes for the starting Player, and announces the starter.
     */
    private void prepareDuel() {
        if (randomize(1, 2) == 1) {
            turn = USER;
        } else {
            turn = CPU;
        }
        System.out.println("A randomly flipped coin has determined that "
                + turn.getName() + " shall have the first turn.");
        startDuel();
    }

    /**
     * The HEART of the duel. Contains a while-loop that cycles until one of the
     * players has no remaining cards.
     */
    private void startDuel() {
        int turnCounter = 0;
        boolean isGameFinished = false;
        Card userCurrent = new Card(), cpuCurrent = new Card();
        Effect userEffect = new Effect(), cpuEffect = new Effect();
        Attack currUserAttack = new Attack(), currCpuAttack = new Attack();
        int maxEff = 0;
        //FOR TESTING - CHOOSE FIRST ATTACK CARD FROM EACH DECK
        for (int x = 0; x < USER.getMainDeck().length; x++) {
            if (USER.getMainDeck()[x].getType() == 0) {
                userCurrent = USER.getMainDeck()[x];
                userEffect = USER.getMainDeck()[x].getAttackList()[0].getEffect(); //FOR TESTING PURPOSES - USING THE FIRST ATTACKS EFFECT
                maxEff = 4;
                break;
            }
        }
        for (int y = 0; y < CPU.getMainDeck().length; y++) {
            if (CPU.getMainDeck()[y].getType() == 0) {
                cpuCurrent = CPU.getMainDeck()[y];
                cpuEffect = CPU.getMainDeck()[y].getAttackList()[0].getEffect();
                break;
            }
        }
        //END TESTING SEGMENT - CHANGE THIS AND ALLOW USER TO CHOOSE HIS CARD,
        //RANDOMIZE CPU CARD
        
        //
        //THE MAIN LOGIC LOOP - separated into two if's
        //        - one for USER turn, and one for CPU turn
        //        - each turn, it completes an attack, then checks if effect is active, if it isn't, 1/5 chance of activating attack
        //        - if  it is, it completes the effect tick, and sets counter down by 1 (if counter is at 1, it completes the effect, then .setActive(false)
        while (!isGameFinished) {
            System.out.println("TURN " + ++turnCounter + ":");
            if (turn == USER) {
                currUserAttack = userCurrent.getAttackList()[0]; //SWITCH THIS TO SELECTED ATTACK, AND NOT AUTOMATICALLY THE 0th ATTACK
                int dmg = currUserAttack.getDmg();
                int cpuHealth = cpuCurrent.getHealth() - dmg;
                cpuCurrent.setHealth(cpuHealth);
                System.out.println(USER.getName() + " has done " + dmg + " damage to " + CPU.getName() + ". "
                        + CPU.getName() + "'s card, \"" + cpuCurrent.getName() + "\" has " + cpuCurrent.getHealth() + " remaining.");
                if (cpuCurrent.getHealth() == 0) {
                    winner = USER;
                    isGameFinished = true;
                    break;
                }
                if (!userEffect.isActive()) {
                    if (randomize(1, 5) == 1) {
                        userEffect.setTurns(maxEff);
                        userEffect.setActive(true);
                        System.out.println(USER.getName() + " has successfully applied an effect! It will apply on " + USER.getName() + "'s next attack.");
                    }
                } else {
                    if (userEffect.getTurns() == 1) {
                        int effDmg = cpuCurrent.getHealth() - userEffect.getAmount();
                        cpuCurrent.setHealth(effDmg);
                        System.out.println(userEffect.getName()+" Effect hits for "+userEffect.getAmount()+". "
                            +CPU.getName()+"'s card, \""+cpuCurrent.getName()+"\" has "+cpuCurrent.getHealth()+" remaining.");
                        userEffect.setActive(false);
                        if (cpuCurrent.getHealth() == 0) {
                            winner = USER;
                            isGameFinished = true;
                            break;
                        }
                    } else {
                        int effDmg = cpuCurrent.getHealth() - userEffect.getAmount();
                        cpuCurrent.setHealth(effDmg);
                        System.out.println(userEffect.getName()+" Effect hits for "+userEffect.getAmount()+". "
                            +CPU.getName()+"'s card, \""+cpuCurrent.getName()+"\" has "+cpuCurrent.getHealth()+" remaining.");
                        if (cpuCurrent.getHealth() == 0) {
                            winner = USER;
                            isGameFinished = true;
                            break;
                        }
                        int t = userEffect.getTurns() - 1;
                        userEffect.setTurns(t);
                    }
                }
                turn = CPU;
            } else if (turn == CPU) {
                int dmg = cpuCurrent.getAttackList()[0].getDmg();
                int userHealth = userCurrent.getHealth() - dmg;
                userCurrent.setHealth(userHealth);
                System.out.println(CPU.getName() + " has done " + dmg + " damage to " + USER.getName() + ". "
                        + USER.getName() + "'s card, \"" + userCurrent.getName() + "\" has " + userCurrent.getHealth() + " remaining.");
                if (userCurrent.getHealth() == 0) {
                    winner = CPU;
                    isGameFinished = true;
                    break;
                }
                if (!cpuEffect.isActive()) {
                    if (randomize(1, 5) == 1) {
                        cpuEffect.setTurns(maxEff);
                        cpuEffect.setActive(true);
                        System.out.println(CPU.getName() + " has successfully applied an effect! It will apply on " + CPU.getName() + "'s next attack.");
                    } 
                } else {
                    if (cpuEffect.getTurns() == 1) {
                        int userH = userCurrent.getHealth();
                        userH -= cpuEffect.getAmount();
                        userCurrent.setHealth(userH);
                        cpuEffect.setTurns(cpuEffect.getTurns() - 1);
                        System.out.println(cpuEffect.getName() + " Effect hits for " + cpuEffect.getAmount() + ". "
                                + USER.getName() + "'s card, \"" + userCurrent.getName() + "\" has " + userCurrent.getHealth() + " remaining.");
                        cpuEffect.setActive(false);
                        if (userCurrent.getHealth() == 0) {
                            winner = CPU;
                            isGameFinished = true;
                            break;
                        }
                    } else {
                        int userH = userCurrent.getHealth();
                        userH -= cpuEffect.getAmount();
                        userCurrent.setHealth(userH);
                        System.out.println(cpuEffect.getName() + " Effect hits for " + cpuEffect.getAmount() + ". "
                                + USER.getName() + "'s card, \"" + userCurrent.getName() + "\" has " + userCurrent.getHealth() + " remaining.");
                        if (userCurrent.getHealth() == 0) {
                            winner = CPU;
                            isGameFinished = true;
                            break;
                        }
                        int y = cpuEffect.getTurns() - 1;
                        cpuEffect.setTurns(y);
                    }
                }
                turn = USER;
            }
        }
        System.out.println("WINNER OF DUEL IS: " + winner.getName());
    }
}
