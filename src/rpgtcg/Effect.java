package rpgtcg;

/**
 * DATA STRUCTURE:
 * Effect class is a lasting effect (turns decided by OnPlayActivity), ie, sleep or paralysis.
 * It can either be a damaging effect (burn/bleed), or controlling effect (paralysis/sleep).
 * @author Moody
 *
 */
public class Effect {
	//INSTANCE VARIABLES
	private int 	type; 	//0 = Damaging effect (ie, burn/bleed) ---- 1 = Heal ----- 2 = Controlling effect)
	private String 	name;// ie; Burn or Sleep
	private int 	amount; // Damage - applies to Type 0 or Type 1 only
	private int		turns; //Number of turns the effect is to last for.
	//NOTE: No turn variable (Declaring how many turns effect lasts), as that will make Paralysis/Bleed, etc, 
	//      all uniform in terms of turns. Thus, that means, paralysis would always last X turns, sleep lasts Y turns, etc.
	//      Keeping 'int turns' in OnCardUse allows us to vary the turns based on the card played.
	//      ie, different attacks that do the same Effect, can have differing turns, based on the power of the card.
	// NOTE 2: TURNS ADDED TO EFFECT CLASS - IGNORE ABOVE - IF THIS DOES NOT WORK, CHANGE IT TO OnCardUse
        private boolean active; //Used for duel testing.
	
	
    /**
     * Empty Constructor. Creates an empty Effect object. Can be filled with SETTER methods. SETS ACTIVE VARIABLE TO FALSE
     */
	public Effect() {
            active = false;
        }
	/**
	 * Effect constructor. Creates an Effect object with the specified parameters. Does not fiddle with 
         * "active" variable, as that is set only by DuelLogic.java, and only used when testing stand alone Effect objects.
	 * @param type 0=Damaging Effect, 1 = Controlling Effect
	 * @param name Effect name (ie, paralysis, burn, etc)
	 * @param amount Only applicable when type=0, the amount of damage incurred per turn.
	 * @param turns Amount of turns the effect is to last for.
	 */
	public Effect(int type, String name, int amount, int turns) {
		this.type = type;
		this.name = name;
		this.amount = amount;
		this.turns = turns;
                active = false;
	}
	
	//GETTER METHODS
	/**
	 * Retrieves the 'type' variable.
	 * @return 0 or 1, depending on damaging/controlling status.
	 */
	public int getType() {return type;}
	/**
	 * Retrieves the 'name' variable
	 * @return Name of effect.
	 */
	public String getName() {return name;}
	/**
	 * Retrieves the amount of damage done by effect per turn.
	 * @return Damage incurred by effect, per turn.
	 */
	public int getAmount() {return amount;}
	
	/**
	 * Retrieves the number of turns the effect is to last for;
	 * @return Number of turns the effect is to last for;
	 */
	public int getTurns() {return turns;}
	//END GETTER METHODS
        
        public boolean isActive() {return active;}
	
	//SETTER METHODS
	/**
	 * Set the type of effect.
	 * @param type 0=Damaging, 1 = Controlling
	 */
	public void setType(int type) {this.type = type;}
	/**
	 * Set the name of the effect.
	 * @param name Effect name
	 */
	public void setName(String name) {this.name = name;}
	/**
	 * Set the amount of damage incurred per turn by effect.
	 * @param amount Damage done per turn by effect.
	 */
	public void setAmount(int amount) {this.amount = amount;}
	
	public void setTurns(int turns) {this.turns = turns;}
	//END SETTER METHODS
        
        public void setActive(boolean active) {this.active = active;}
}
