package rpgtcg;


/** 
 * DATA STRUCTURE:
 * A class that deals with an individual attack. Contains an attack name, and damage of that attack.
 * @author Moody
 *
 */

//TO-DO:
//    - CHANGE TO ABILITY CLASS?
public class Attack {
    //INSTANCE VARIABLE DECLARATION - ADD int TYPE - to allow for healing abilities -
    private String 	attName;
    private int 	attDmg;
    private Effect	effect;


    /**
     * Empty Attack constructor. Use with SETTER methods.
     */
    public Attack() {}
    /**
     *  Attack Constructor. Creates an Attack object.
     * @param attName Name of attack
     * @param attDmg Damage dealt by attack
     */
    public Attack(String attName, int attDmg, Effect effect) {
        this.attName = attName;
        this.attDmg = attDmg;
        this.effect = effect;
    }

    //GETTER METHODS
    /**
     * Retrieves name of attack.
     * @return The name of the attack.
     */
    public String getName() {return attName;}
    
    /**
     * Retrieves amount of damage done by attack.
     * @return Damage dealt by attack.
     */
    public int getDmg() {return attDmg;}
    
    /**
     * Retrieves Effect of attack
     * @return Effect that follows the attack (ie, bleed, paralysis, etc).
     */
    public Effect getEffect() {return effect;}
    //END GETTER METHODS

    //SETTER METHODS
    /**
     * Sets the name of the attack
     * @param attName Intended name of the attack.
     */
    public void setName(String attName) {this.attName = attName;}
    
    /**
     * Sets the amount of damage done by the attack.
     * @param attDmg Intended damage amount of attack.
     */
    public void setDmg(int attDmg) {this.attDmg = attDmg;}
    
    /**
     * Sets the Effect of the attack;
     * @param effect Intended effect of the attack. (ie, bleed, paralysis, etc);
     */
    public void setEffect(Effect effect) {this.effect = effect;}
    //END SETTER METHODS
}