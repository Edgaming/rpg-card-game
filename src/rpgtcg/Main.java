package rpgtcg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * The logic.
 * @author Moody
 */
public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	
    //	THIS IS USED TO TEST CARD READER - SO FAR IT WORKS PROPERLY! :D
        /*
    	CardReader cr = new CardReader();
    	String[] lines = cr.getLines();
    	Card[] mainDeck = new Card[10];
    	for(int x = 0; x < lines.length; x++) {
    		if (lines[x] != null) {
    			mainDeck[x] = cr.parseCard(lines[x]);
    			for(int y = 0; y<3; y++) { //Hardcoded 3 because each character has 3 attacks. CHANGE THIS. HAVE A CLASS-WIDE VARIABLE THAT STIPULATES MAX NUMBER OF ATTACKS
        			System.out.println(mainDeck[x].getAttackString(y));
    			}
    		}
    	}
    	
    	//Testing output retrieving ONLY information from Card objs (ALL card objs are read in through Card Reader).
    	int calc = mainDeck[0].getHealth() - mainDeck[1].getCardEffect().getAmount();
    	System.out.println("\n\n"+mainDeck[0].getCardEffect().getName() + " affects you for " 
    			+ mainDeck[1].getCardEffect().getAmount()+" damage.\nYou have "+calc+ " health remaining.");
    	calc -= mainDeck[0].getAttackList()[2].getDmg();
    	System.out.println(mainDeck[0].getAttackString(2)+"\nYou have " +calc+" health remaining.");
    */
        Player p1 = new Player();
        p1.setClassId(0);
        
        p1.setClassName("Fighter");
        p1.setLevel(2);
        p1.setName("Oxid");
        AttrSet attrs = new AttrSet(10,5,10,5);
        p1.setAttrSet(attrs);


        //PLAYER ONE - FIRST CARD
        Card cardOne = new Card();
        cardOne.setName("Hammersmith");
        cardOne.setType(0);
        cardOne.setHealth(1250);
        Attack[] aList = new Attack[2];
        Effect effect1 = new Effect(0, "Wounds open from a vicious hammer strike!", 35, 4);
        Effect effect2 = new Effect(1, "Lingering adrenaline returns health to the attacker!", 30, 2);
        Attack at1 = new Attack("Hammer Smash", 150, effect1);
        Attack at2 = new Attack("Hammer Rush", 75, effect2);
        aList[0] = at1; 
        aList[1] = at2;
        cardOne.setAttackList(aList);
        //cardOne.setCardEffect(XXX);

        //PLAYER ONE - SECOND CARD
        Card cardTwo = new Card();
        cardTwo.setName("Ninjitsu");
        cardTwo.setType(0);
        cardTwo.setHealth(300);
        Attack[] aList2 = new Attack[2];
        Effect effect3 = new Effect(0, "Poison!", 40, 2);
        Effect effect4 = new Effect(2, "Paralysis!",0, 2);
        Attack at3 = new Attack("Sword Slash", 75, effect3);
        Attack at4 = new Attack("Paralyze!", 50, effect4);
        aList2[0] = at3;
        aList2[1] = at4;
        cardTwo.setAttackList(aList2);
        //PLAYER ONE - SETTING PLAYER
        Card[] deck1 = {cardOne, cardTwo};
        p1.setMainDeck(deck1);



   //PLAYER TWO - INIT
        Player p2 = new Player();
        p2.setClassId(0);
        p2.setClassName("Magician");
        p2.setLevel(4);
        p2.setName("Skipio");
        AttrSet attrs2 = new AttrSet(5,5,15,5);
        p2.setAttrSet(attrs2);
//PLAYER TWO - FIRST CARD
        Card card1 = new Card();
        card1.setName("Firethrower");
        card1.setType(0);
        card1.setHealth(1000);
        Effect eff = new Effect(0, "Foe's flames burn you!", 50, 3);
        Effect eff1 = new Effect(1, "Foe's warmth and passion grants him health!", 35, 4);
        Attack a1 = new Attack("Fireball", 200, eff);
        Attack a2 = new Attack("Warmth", 0, eff1);
        Attack[] list = {a1, a2};
        card1.setAttackList(list);
//PLAYER TWO - SECOND CARD
        Card card2 = new Card();
        card2.setName("Ice Weaver");
        card2.setType(0);
        card2.setHealth(450);
        Effect effect = new Effect(0, "Hypothermia takes its toll!", 40, 4);
        Effect effectOne = new Effect(2, "Foe's ice prevents you from moving!", 0, 2);
        Attack aOne = new Attack("Frost Bolt", 75, effect);
        Attack aTwo = new Attack("Freeze!", 50, effectOne);
        Attack[] list1 = {aOne, aTwo};
        card2.setAttackList(list1);

        Card[] deck2 = {card1, card2};
        p2.setMainDeck(deck2);

       
    	DuelLogic duel = new DuelLogic(p1, p2);
    
    }
}
