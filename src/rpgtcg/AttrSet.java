package rpgtcg;

/** 
 * DATA STRUCTURE:
 * A set of attributes (containing 4 attributes - possibly 2 more [ VIT & INT ] ).
 * These values determine the players effectiveness with certain areas in battle,
 * also, increases the luck of the player (for dice rolls/coin tosses).
 * @author Moody
 */

//TO DO - ADD END AND MEM METHODS - FIX EVERYTHING ELSE THAT USES ATTRSET CONTRUCTOR
public class AttrSet {
	/**
	 * The maximum allowed STRENGTH for a Player.
	 */
	public final static int MAX_STR = 50;
	/**
	 * The maximum allowed AGILITY for a Player.
	 */
	public final static int MAX_AGI = 50;
	/**
	 * The maximum allowed INTELLIGENCE for a Player.
	 */
	public final static int MAX_INT = 50;
	/**
	 * The maximum allowed LUCK for a Player.
	 */
	public final static int MAX_LUCK = 50;
	
	
	//INSTANCE VARIABLE DECLARATION
    private int END; //Endurance - health
    private int MEM; //Memory - mana resource for ALL classes
    private int STR; //Strength
    private int AGI; //Agility
    private int INT; //Intelligence
    private int LUCK;//Luck

    /**
     * Empty Constructor. Use with SETTER methods. 
     */
    public AttrSet() {}
    /**
     * AttrSet Constructor. Creates an AttrSet object.
     * @param STR Strength attribute.
     * @param AGI Agility attribute.
     * @param INT Intelligence attribute.
     * @param LUCK Luck attribute.
     */
    public AttrSet(int STR, int AGI, int INT, int LUCK) { //NOTE: S.A.I.L abbreviation
    	this.STR = STR;
    	this.AGI = AGI;
    	this.INT = INT;
    	this.LUCK = LUCK;
    }

    //GETTER METHODS
    /**
     * Retrieves the Strength attribute of Player.
     * @return Strength attr. of player.
     */
    public int getSTR() {return STR;}
    /**
     * Retrieves the Agility attribute of Player.
     * @return Agility attr. of player.
     */
    public int getAGI() {return AGI;}
    /**
     * Retrieves the Intelligence attribute of Player.
     * @return Intelligence attr. of player.
     */
    public int getINT() {return INT;}
    /**
     * Retrieves the Luck attribute of Player.
     * @return Luck attr. of player.
     */
    public int getLUCK() {return LUCK;}
    //END GETTER METHODS

    //SETTER METHODS
    /**
     * Sets the Strength attribute of Player.
     * @param STR Strength attribute.
     */
    public void setSTR(int STR) {this.STR = STR;}
    /**
     * Sets the Agility attribute of Player.
     * @param AGI Agility attribute.
     */
    public void setAGI(int AGI) {this.AGI = AGI;}
    /**
     * Sets the Intelligence attribute of Player.
     * @param INT Intelligence attribute.
     */
    public void setINT(int INT) {this.INT = INT;}
    /**
     * Sets the Luck attribute of Player.
     * @param LUCK Luck attribute.
     */
    public void setLUCK(int LUCK) {this.LUCK = LUCK;}
    //END SETTER METHODS
}
