/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgtcg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Moody
 */
public class Duel {

    private Player p1, p2;
    private Player startPl, nextPl;

    // private BufferedReader br;
    public Duel(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        intro();
    }

    public void intro() {
        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);

        int answer = 0;
        System.out.println("Welcome, " + p1.getName() + "!");
        System.out.println("You are about to begin a duel with " + p2.getName()
                + ". Begin? (Y/N):");
        try {
            answer = br.read();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        if (answer == 0) {
            System.out.println("Nothing read");
        } else if ((char) answer == 'Y' || (char) answer == 'y') {
            System.out.println("PLEASE PROCEED!");
            startDuel();
        } else if ((char) answer == 'N' || (char) answer == 'n') {
            System.out.println("Why would you start then?!");
        } // THEN QUIT
        else {
            System.out.println("Input not recognized.");
        }
        // try {
        // br.close();
        // is.close();
        // } catch (IOException ex) {
        // Logger.getLogger(Duel.class.getName()).log(Level.SEVERE, null, ex);
        // }

    }

    private void startDuel() {
        flipForStart();
        // If p1 is start, present him with options
        if (startPl.getName().equals(p1.getName())) {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    System.in));
            prepareOptions(br);
        } else { // p2 is start - create AI choices
            // TO-DO: AI - chose card and attack.
        }
    }

    private void flipForStart() {
        double rand = randomize();
        if (rand > 1) {
            startPl = p1;
            nextPl = p2;
        } else {
            startPl = p2;
            nextPl = p1;
        }
        System.out.println("A randomly flipped coin has determined that "
                + startPl.getName() + " shall have the first turn.");
    }

    private double randomize() {
        double r = 0;
        r = Math.random();
        return r * 2;
    }

    private void prepareOptions(BufferedReader br) {
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader buff = new BufferedReader(in);

        String choice = "0";

        System.out.println("Choose an option (1/2/3): ");
        System.out.println("1. Choose Card");
        System.out.println("2. View Deck");
        System.out.println("3. Use Card");

        try {
            choice = br.readLine();
        } catch (IOException io) {
            System.out.println(io.getMessage().toString());
        }

        System.out.println("CHOICE: " + choice);
        if (choice.equals("1")) {
            chooseCard();
        } else if (choice.equals("2")) {
            viewDeck();
        } else if (choice.equals("3")) {
            useCard();
        } else if (choice.equals("0")) {
            System.out.println("IT'S STILL 0 DUMBASS");
        }

        // try{
        // br.close();
        // } catch(IOException io) {
        // System.out.println(io.getMessage());
        // }
    }

    private void chooseCard() {
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader buff = new BufferedReader(in);
        String cardChoice = "0";
        Card[] deck = p1.getMainDeck();
        for (int x = 0; x < deck.length; x++) {
            int choiceNum = x + 1;
            System.out.println(choiceNum + ". " + deck[x].getName());
        }
        System.out.println("Which card would you like to see?: ");
        try {
            cardChoice = buff.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int c = Integer.parseInt(cardChoice) - 1;
        readCard(c);
    }

    private void viewDeck() {
    }

    private void useCard() {
    }

    private void readCard(int choice) {
        System.out.println("YOU CHOSE: " + p1.getMainDeck()[choice].getName());
    }
}
