package rpgtcg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Class that deals with reading the .txt files to load in the card values for
 * all the Data Structures. Contains accessible public methods. Will be the
 * class that is called to load in all card values, so Card obj creation is
 * here.
 *
 * Only this class (regarding Card/OnPlayActivity/Effect) should use a
 * BufferedReader.
 *
 * @author Moody
 *
 */
public class CardReader {
    //INSTANCE VARIABLE DECLARATION

    private BufferedReader br;
    private String[] lines = new String[10]; //MAX CARD value?!
    private String testStr;

    /**
     * Empty constructor, as no variables are needed. Object must be constructed
     * to call its methods.
     */
    public CardReader() {
    }

    /**
     * Retrieves all the lines (ie, card) from the txt file.
     *
     * @return Array of String containing all the lines from CardData.txt
     */
    public String[] getLines() {
        try {
            br = new BufferedReader(new FileReader("C:/Users/Moody/EclipseJavaWork/TradingCardGame/src/tradingcardgame/CardData.txt"));
            int x = 0;
            while ((testStr = br.readLine()) != null) { //Testing the loop condition also reads the line, and sets it to testStr string.
                lines[x] = testStr;
                x++;
            }
            br.close();
        } catch (IOException i) {
            System.out.println(i.getMessage());
        }
        return lines;
    }

    /**
     * Function that parses a Card object from a String. String must be in
     * proper format. (ie, 14 fields separated by commas). Function logic will
     * dictate and fill in the values of the Card object.
     *
     * @param line String containing the line we wish to parse into a Card
     * object.
     * @return Card object containing the information in the line.
     */
    public Card parseCard(String line) {
        //INDEXES USED IN cardInfo ARRAY IN SETTER METHODS ARE HARDCODED, BECAUSE THAT IS HOW THE DATA IS SAVED.
        String[] cardInfo = line.split(", ");
        Card card = new Card();
        card.setName(cardInfo[0]);
        card.setType(Integer.parseInt(cardInfo[1]));
        card.setHealth(Integer.parseInt(cardInfo[2]));
        Attack[] atks = {new Attack(cardInfo[3], Integer.parseInt(cardInfo[4]), new Effect()),
            new Attack(cardInfo[5], Integer.parseInt(cardInfo[6]), new Effect()),
            new Attack(cardInfo[7], Integer.parseInt(cardInfo[8]), new Effect())};
        card.setAttackList(atks);
        card.setCardEffect(new Effect(Integer.parseInt(cardInfo[11]), cardInfo[12], Integer.parseInt(cardInfo[13]), Integer.parseInt(cardInfo[14])));
        return card;
    }
}
