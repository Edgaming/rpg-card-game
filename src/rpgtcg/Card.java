package rpgtcg;

/** 
 * DATA STRUCTURE: 
 * A single card. Each card contains information, and can either be
 * a FIGHTING (character) card, or a USE (bonus effect on play) card.
 *
 * This class also contains the Attack class. This class is used to
 * condense the information of the attacks, as only a Card object will
 * be accessing information from an Attack object.
 * @author Moody
 */

//TO DO: 
//   - MUST ADD HEALTH TO TYPE 1 (FIGHTER) CARDS.
//   - ADD CARD EFFECT (when played)
public class Card {
    //INSTANCE VARIABLE DECLARATION
    private String 			name;
    private int 			type;
    private int 			health;
    private Attack[] 		attacks;
    private Effect 		onPlay; //Activities activated by playing the card.
    
    /**
     * Empty Constructor. Use with SETTER methods.
     */
    public Card() {}
    /**
     * Card Constructor. Creates a Card object.
     * @param name Name of the card
     * @param type 0=Character Card, 1=Use Card; ??2 = Energy Card??
     * @param health Applies to card if type=0. The amount of health the character card has.
     * @param attacks A list of Attack objects, that reference each attack (name, and damage).
     * @param onPlay OnPlayActivity object that references (if any, could be empty) an activity that is triggered when the card is played.
     */
    public Card(String name, int type, int health, Attack[] attacks, Effect onPlay) {
        this.name = name;
        this.type = type;
        this.health = health;
        this.attacks = attacks;
        this.onPlay = onPlay;
    }

    //GETTER METHODS
    /**
     * Retrieves the name of the card.
     * @return Name of card.
     */
    public String getName() {return name;}
    /**
     * Retrieves the Type of the card.
     * @return Integer that corresponds with the type of the card in question.
     */
    public int getType() {return type;}
    /**
     * Retrieves the Health (0 if not applicable) of card.
     * @return Health of card, or 0 if not applicable.
     */
    public int getHealth() {return health;}
    /**
     * Retrieves the list of Attack objects. Used primarily for internal testing.
     * @return The list of Attack objects that the card references.
     */
    public Attack[] getAttackList() {return attacks;}
    /**
     * Retrieves the OnPlayActivity object (if any) of the card. The Activity is what is triggered when card is played.
     * @return OnPlayActivity object that the card references.
     */
    public Effect getCardEffect() {return onPlay;}
    
    //GETTER - Non-class variables.
    /**
     * Retrieves the display String (for easy access). Will be in format: A does B damage with C attack.
     * @param x Index of Attack. This Method references Attack[], thus an index is required for the Attack to display.
     * @return A String that contains "A does B damage with C." Where A is the character card name, B is the amount, and C is the attack name.
     */
    public String getAttackString(int x) { //This method retrieves the display string, after the attack. (Attack X does XY damage)
    	if(x > attacks.length) { //Test that the index received is not bigger than the
    		System.out.println("ERROR at getAttackString() - index larger than array length"); //TEST STRING - REMOVE
    		return "error";
    	} 
    	else return name+" does "+attacks[x].getDmg()+" damage with "+attacks[x].getName()+".";
    }
    //END ALL GETTER METHODS

    //SETTER METHODS
    /**
     * Sets the Name of the card.
     * @param name Intended new name of the card.
     */
    public void setName(String name) {this.name = name;}
    /**
     * Sets the Type of the card. 0=Character Card, 1=Use Card.
     * @param type Intended new type of the card.
     */
    public void setType(int type) {this.type = type;}
    /**
     * Sets the amount of Health the card has. Only applicable if "type=0", if it is a Use Card ("type=1"), then health should be 0. 
     * Use when buffing health of a card, due to an Effect, or OnPlayActivity.
     * @param health Intended new health amount of card.
     */
    public void setHealth(int health) {
        //IF A CARDS HEALTH IS SET BELOW 0, JUST KEEP IT AT 0 FOR TESTING PURPOSES;
        if(health<0)
            this.health = 0;
        else
            this.health = health;
    }
    /**
     * Sets the list of Attack objects (Attack array) the Character Card has. (Only applicable if "type=0"). Use when reading Cards from txt.
     * @param attacks List of Attack objects that the character card is capable of using.
     */
    public void setAttackList(Attack[] attacks) {this.attacks = attacks;}
    /**
     * Sets the OnPlayActivity object of the card. The object that references if there are any actions to complete when the card is played. (ie, a heal from a potion).
     * @param onPlay A new OnPlayActivity object that the card should reference.
     */
    public void setCardEffect(Effect onPlay)	{this.onPlay = onPlay;}
    //END SETTER METHODS
}
