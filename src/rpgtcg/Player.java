package rpgtcg;

/** 
 * DATA STRUCTURE:
 * The player. All information regarding the player. This is the highest hierarchical data structure,
 * and it contains all other data structures (children, and grand children of this class); 
 * ie, A Player object uses a collection of Card objects (card array), and each Card object uses a 
 *     collection of Attack objects (attack array). Player also uses AttrSet objects, but not related to Card/Attack/Effect.
 * @author Moody
 */
public class Player {
    private int 	lvl;
    private String 	name;
    private int 	classID; //classID used for internal testing (ints easier than strings for testing)
    private String 	className; //Display purposes.
    private AttrSet attrs;   //PLAYER ATTRIBUTES - AFFECTS DAMAGE/HEALTH, ETC.
    private Card[] 	mainDeck; //MAIN HAND
    private Card[] 	unDeck;   //CARDS NOT IN PLAY, BUT OWNED

    /**
     * Empty Constructor. Creates an empty Player object. Use with SETTER methods.
     */
    public Player() {}
    /**
     * Player Constructor. Creates a Player object with the following information.
     * @param lvl Level of the Player.
     * @param name Name of the Player.
     * @param classID Number that indicates the class of the Player.
     * @param className Name of the class of the Player.
     * @param attrs AttrSet object that references the attributes of the Player.
     * @param mainDeck Collection of Card objects (Card array) that is the users main/active deck to be used in duels and such.
     * @param unDeck Collection of Card objects (Card array) that the Player owns, but is NOT in the main/active deck.
     */
    public Player(int lvl, String name, int classID, String className, AttrSet attrs, Card[] mainDeck, Card[] unDeck) {
    	this.lvl = lvl;
    	this.name = name;
    	this.classID = classID;
    	this.className = className;
    	this.attrs = attrs;
    	this.mainDeck = mainDeck;
    	this.unDeck = unDeck;
    }

    //GETTER METHODS
    /**
     * Retrieve the level of the player.
     * @return Integer that corresponds to the Player level.
     */
    public int getLevel() 		{return lvl;}
    /**
     * Retrieve the classID of the Player. ID corresponds to a class name, but is used for testing.
     * @return Integer that corresponds to the classID of the Player.
     */
    public int getClassID() 	{return classID;}
    /**
     * Retrieve the name of the Player.
     * @return String containing the name of the Player.
     */
    public String getName() 	{return name;}
    /**
     * Retrieve the class name of the Player.
     * @return String containing the class name of the Player.
     */
    public String getClassName(){return className;}
    /**
     * Retrieve the Player's AttrSet object, that contains information regarding the Player's attributes.
     * @return AttrSet object containing the Player's attributes.
     */
    public AttrSet getAttrSet() {return attrs;}
    /**
     * Retrieves a list of Card objects that is Player's main dueling hand.
     * @return List of Card objects that is the Player's main deck.
     */
    public Card[] getMainDeck() {return mainDeck;}
    /**
     * Retrieves a list of all the Card objects the Player owns, but does not have included in their main hand. 
     * @return Returns the list of all Card objects the Player owns, but does not include in their main hand.
     */
    public Card[] getUnDeck() 	{return unDeck;}
    //END GETTER METHODS

    //SETTER METHODS
    /**
     * Sets the level of the Player.
     * @param lvl The intended new level of the Player.
     */
    public void setLevel(int lvl) {this.lvl = lvl;}
    /**
     * Sets the classID of the Player.
     * @param classID The intended new classID of the Player.
     */
    public void setClassId(int classID) {this.classID = classID;}
    /**
     * Sets the name of the Player.
     * @param name The intended new name of the Player.
     */
    public void setName(String name) {this.name = name;}
    /**
     * Sets the class name of the Player.
     * @param className The intended new class name of the Player.
     */
    public void setClassName(String className) {this.className = className;}
    /**
     * Sets the Attribute Set (AttrSet class) for the player.
     * @param attrs AttrSet object containing the attributes for the player.
     */
    public void setAttrSet(AttrSet attrs) {this.attrs = attrs;}
    /**
     * Sets the list of Card objects (Card array) that is the Player's main deck.
     * @param mainDeck The intended new list of Card objects (Card array) of the Player.
     */
    public void setMainDeck(Card[] mainDeck) {this.mainDeck = mainDeck;}
    /**
     * Sets the list of Card Objects (Card array) that are NOT in the Player's main deck, but owned by the Player.
     * @param unDeck The intended new list of Card objects (Card array) that don't include the mainDeck, of the Player.
     */
    public void setUnDeck(Card[] unDeck) {this.unDeck = unDeck;}
    //END SETTER METHODS

}
